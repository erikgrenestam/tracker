import boto3
from botocore.exceptions import ClientError
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart


def send_graph(recipients, attachments):
    client = boto3.client(
        'ses',
        region_name='eu-west-1',
        aws_access_key_id='something',
        aws_secret_access_key='something'
    )

    message = MIMEMultipart()
    message['Subject'] = 'attachment test'
    message['From'] = 'erik@erikgrenestam.se'
    message['To'] = ', '.join(recipients)
    # message body
    part = MIMEText('Sending an attachment from AWS', 'html')
    message.attach(part)
    # attachment
    #if attachment_string:   # if bytestring available
    #    part = MIMEApplication(str.encode('attachment_string'))
    for a in attachments:
        part = MIMEApplication(open(a, 'rb').read())
        part.add_header('Content-Disposition', 'attachment', filename=a)
        message.attach(part)

    response = client.send_raw_email(
        Source=message['From'],
        Destinations=recipients,
        RawMessage={
            'Data': message.as_string()
        }
    )