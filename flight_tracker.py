import warnings
from datetime import timedelta, datetime, date, time
from traffic.data import airports, opensky, aircraft
import glob
from matplotlib import dates
from cycler import cycler
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
#from aws_email import send_graph
#import boto3
#import logging

opensky.username = USER
opensky.password = PASS

# Most warnings are not related to the traffic library
warnings.simplefilter("ignore", DeprecationWarning)

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield (start_date + timedelta(n), start_date + timedelta(n + 1))


def append_pickles(name):
    filelist =  glob.glob(f'flights/{name}_flights*.pkl')
    dfs = []
    for file in filelist:
        dfs.append(pd.read_pickle(file))
    df = pd.concat(dfs, sort=False)
    return df.reset_index(drop=True)

def define_nb_colors():
    """
    Defines Nationalbankens' colors and update matplotlib to use those as default
    """
    c = cycler(
        'color',
        [
            (0 / 255, 123 / 255, 209 / 255),
            (146 / 255, 34 / 255, 156 / 255),
            (196 / 255, 61 / 255, 33 / 255),
            (223 / 255, 147 / 255, 55 / 255),
            (176 / 255, 210 / 255, 71 / 255),
            (102 / 255, 102 / 255, 102 / 255)
        ])
    plt.rcParams["axes.prop_cycle"] = c
    colors = [i['color'] for i in c.__dict__['_left']]
    return colors


def plot_figure(df, name):
    _, ax = plt.subplots(1, 1, figsize=(14, 6))
    df['daynum'] = dates.date2num(df.index)

    colors = define_nb_colors()
    days = dates.DayLocator(interval=1)
    fmt = dates.DateFormatter('%d/%m')

    #ax = sns.lineplot(x="daynum", y="callsign_to", data=df, ax=ax, sort=False, color=colors[0])
    ax = sns.lineplot(x="daynum", y="callsign_passenger", data=df, ax=ax, sort=False, color=colors[0])
    ax = sns.lineplot(x="daynum", y="callsign_freight", data=df, ax=ax, sort=False, color=colors[1])
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(fmt)

    ax.set_ylabel(f"Number of take-offs from {name}")
    ax.set_xlabel("")
    ax.legend(labels=['Passenger', 'Freight'], fancybox=False, frameon=False)
    #plt.axvspan(dates.date2num(pd.to_datetime('2020-03-13 19:00:00')), dates.date2num(pd.to_datetime('2020-03-13 19:38:00')), color=colors[5], alpha=0.5, lw=0)
    #plt.annotate('Pressemøde i  \n Statsministeriet  ', (dates.date2num(pd.to_datetime('2020-03-13 19:00:00')), 120), ha = 'right', color=colors[5])
    weekend_indices = find_weekend_indices(df.index + pd.DateOffset(days=1))
    # highlight areas
    highlight_weekends(weekend_indices, ax)

    plt.axvspan(dates.date2num(pd.to_datetime('2020-03-15 15:43:00')),
                dates.date2num(pd.to_datetime('2020-03-15 18:50:00')), color=colors[5], alpha=0.5, lw=0)
    plt.annotate('German plans to close  \nborders with DK leak. \nSAS announces 90% temp layoffs.',
                 (dates.date2num(pd.to_datetime('2020-03-15 15:45:00')), 0.8), xycoords=('data', 'axes fraction'), ha='right', color=colors[5])

    ax.annotate(f'Data shows an approximate count of the daily number of commercial take-offs from {name}. Data is collected daily until 23.59. Weekends are shaded. \nSource: The OpenSky Network, http://www.opensky-network.org' ,
                xy=(0, 0), xytext=(0, 10),
                xycoords=('axes fraction', 'figure fraction'),
                textcoords='offset points',
                size=10, ha='left', va='bottom')

    plt.gcf().autofmt_xdate()
    ax.set_title(f'Number of commericial flights from {name}')
    plt.tight_layout()
    plt.savefig(f'{name}_plot.pdf', dpi=200, transparent=True)
    return

def get_flights(start_date, end_date, airport_icao, name):
    not_passenger = ['EAT Leipzig', 'Norsk Luftambulanse', 'Royal Air Force', 'Cargo Air', 'FedEx',
               'Farnair Hungary', 'Fly 7 Executive Aviation SA', 'DHL', 'Emirates SkyCargo']

    freight = ['EAT Leipzig', 'Cargo Air', 'FedEx',
               'Farnair Hungary', 'DHL', 'Emirates SkyCargo']

    for single_date in daterange(start_date, end_date):
        start = single_date[0].strftime("%Y-%m-%d %H:%M")
        end = single_date[1].strftime("%Y-%m-%d %H:%M")
        print(start, end)
        flights = opensky.flightlist(
          start,
          end,
          airport=airport_icao)
        startstr = single_date[0].strftime("%Y-%m-%d")
        flights.to_pickle(f'flights/{name}_flights_' + startstr + '.pkl')

    df = append_pickles(name)
    acraft = pd.read_csv("data/aircraft_db.csv")
    acraft2 = pd.read_csv("data/aircraft2.csv", sep=';')
    acraft2['icao'] = acraft2['icao'].str.lower()
    df = df.merge(acraft, left_on="icao24", right_on="icao", how='left')
    df = df.merge(acraft2, left_on="mdl", right_on="icao", how='left')
    df_freight = df[df.operator.isin(freight)]
    df = df[~df.operator.isin(not_passenger)]
    print(airport_icao)

    df = df.loc[df['origin'] == airport_icao].dropna(subset=['mdl'])
    df = df.groupby('day').agg('count')

    df_freight = df_freight.groupby('day').agg('count')

    #df_to = df.loc[df['destination'] == airport_icao].dropna(subset=['mdl'])
    #df_to = df_to.groupby('day').agg('nunique').drop(columns=['day']).reset_index()

    df = df.join(df_freight, lsuffix='_passenger', rsuffix='_freight')

    plot_figure(df, name)
    df[['callsign_passenger', 'callsign_freight']].to_csv(f"{name}_flights.csv")
    return df

def find_weekend_indices(datetime_array):
    indices = []
    for i in range(len(datetime_array)):
        if datetime_array[i].weekday() == 5:
            indices.append(dates.date2num(datetime_array[i]))
    return indices

def highlight_weekends(indices, ax):
    i = 0
    while i < len(indices):
        ax.axvspan(indices[i], indices[i]+2, facecolor='grey', edgecolor='none', alpha=.2)
        i += 1

if __name__ == '__main__':

    logger = logging.getLogger()

    start_date = datetime(2020, 2, 15, 23, 59)
    end_date = datetime.combine((date.today() - timedelta(days=1)), time(23, 59))

    #bll = get_flights(start_date, end_date, 'EKBI', 'Billund')
    cph = get_flights(start_date, end_date, 'EKCH', 'CPH')
    #aal = get_flights(start_date, end_date, 'EKYT', 'AAL')
    send_graph(['egr@nationalbanken.dk'], ['CPH_plot.pdf', 'CPH_flights.csv'])

    #ec2 = boto3.client('ec2', region_name='eu-north-1')
    #ec2.stop_instances(InstanceIds=['i-041a2164131815e71'])

